/**
 * GNU GENERAL PUBLIC LICENSE See the file license.txt for copying conditions.
 * 
 * @author mjwhitta
 * @created Dec 31, 2012
 */
package sites.mjwhitta.andlib.mjwhitta.utils;

/**
 * This class provides some easy of use methods for strings
 */
public class StringUtils {
   public static boolean isNullorEmpty(String str) {
      if ((str != null) && (str != "")) {
         return false;
      }
      return true;
   }
}
